import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    //Koostada meetod, mis leiab etteantud sidusas lihtgraafis etteantud tipust v kõige kaugemal asuva tipu.

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        /* EXAMPLE: TWO VERTECES */
        Graph g1 = new Graph("Simple graph example:");
        //verteces
        g1.createVertex("v1");
        g1.createVertex("v2");
        //v1 arcs
        g1.createArc("av1_v2", g1.getVertexById("v1"), g1.getVertexById("v2"));
        //v2 arcs
        g1.createArc("av2_v1", g1.getVertexById("v2"), g1.getVertexById("v1"));
        //results
        System.out.println(g1.toString());
        System.out.println("v1 kõige kaugemad tipud: " + g1.farthestVertex(g1.getVertexById("v1")));
        System.out.println("v2 kõige kaugemad tipud: " + g1.farthestVertex(g1.getVertexById("v2")));
        System.out.println("****************************");

        /* EXAMPLE: MULTIPLE RESULTS */
        Graph g2 = new Graph("Multiple results example:");
        //verteces
        g2.createVertex("v1");
        g2.createVertex("v2");
        g2.createVertex("v3");
        //v1 arcs
        g2.createArc("av1_v2", g2.getVertexById("v1"), g2.getVertexById("v2"));
        g2.createArc("av1_v3", g2.getVertexById("v1"), g2.getVertexById("v3"));
        //v2 arcs
        g2.createArc("av2_v1", g2.getVertexById("v2"), g2.getVertexById("v1"));
        g2.createArc("av2_v3", g2.getVertexById("v2"), g2.getVertexById("v3"));
        //v3 arcs
        g2.createArc("av3_v1", g2.getVertexById("v3"), g2.getVertexById("v1"));
        g2.createArc("av3_v2", g2.getVertexById("v3"), g2.getVertexById("v2"));
        //results
        System.out.println(g2.toString());
        System.out.println("v1 kõige kaugemad tipud: " + g2.farthestVertex(g2.getVertexById("v1")));
        System.out.println("v2 kõige kaugemad tipud: " + g2.farthestVertex(g2.getVertexById("v2")));
        System.out.println("v3 kõige kaugemad tipud: " + g2.farthestVertex(g2.getVertexById("v3")));

        /* EXAMPLE: MULTIPLE PATHS */
        Graph g3 = new Graph("Multiple paths graph example:");
        //verteces
        g3.createVertex("v1");
        g3.createVertex("v2");
        g3.createVertex("v3");
        g3.createVertex("v4");
        //v1 arcs
        g3.createArc("av1_v2", g3.getVertexById("v1"), g3.getVertexById("v2"));
        g3.createArc("av1_v3", g3.getVertexById("v1"), g3.getVertexById("v3"));
        //v2 arcs
        g3.createArc("av2_v1", g3.getVertexById("v2"), g3.getVertexById("v1"));
        g3.createArc("av2_v3", g3.getVertexById("v2"), g3.getVertexById("v3"));
        g3.createArc("av2_v4", g3.getVertexById("v2"), g3.getVertexById("v4"));
        //v3 arcs
        g3.createArc("av3_v1", g3.getVertexById("v3"), g3.getVertexById("v1"));
        g3.createArc("av3_v2", g3.getVertexById("v3"), g3.getVertexById("v2"));
        //v4 arcs
        g3.createArc("av4_v2", g3.getVertexById("v4"), g3.getVertexById("v2"));
        //results
        System.out.println(g3.toString());
        System.out.println("v1 kõige kaugemad tipud: " + g3.farthestVertex(g3.getVertexById("v1")));
        System.out.println("v2 kõige kaugemad tipud: " + g3.farthestVertex(g3.getVertexById("v2")));
        System.out.println("v3 (näidis) kõige kaugemad tipud: " + g3.farthestVertex(g3.getVertexById("v3")));
        System.out.println("v4 kõige kaugemad tipud: " + g3.farthestVertex(g3.getVertexById("v4")));
        System.out.println("****************************");

        /* EXAMPLE: PERFORMANCE */
        Graph g4 = new Graph("Performance test");
        g4.createRandomSimpleGraph(2500, 3123750);
        long startTime4 = java.lang.System.nanoTime();
        List results4 = g4.farthestVertex(g4.getVertexById("v1250"));
        long difference4 = java.lang.System.nanoTime() - startTime4;
        System.out.println("Performance test: ");
        System.out.println("Otsimise aeg: " +
                String.format("%d ms", TimeUnit.NANOSECONDS.toMillis(difference4)));

    }

    /**
     * Vertex represents one vertex in the graph. Vertex has unique id,
     * atleast one arc that leads to certain next vertex (if there are
     * more than one verteces in graph). Vertex can have
     * on default next vertex. Vertex has fixed the possition in
     * adjacency matrix and distance from any other vertex.
     */
    public class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        private boolean visited = false;
        private int distance = 0;

        /**
         * Constructor of vertex with all parameters specialized.
         *
         * @param s as id of the vertex
         * @param v as default next vertex
         * @param e as arc between the vertex and default next
         *          vertex
         */
        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        /**
         * Constructor of a vertex with only id specialized.
         *
         * @param s as id of the string
         */
        Vertex(String s) {
            this(s, null, null);
        }

        /**
         * Returns a string representation of the vertex.
         *
         * @return id as id of the vertex
         */
        @Override
        public String toString() {
            return this.id;
        }

        public Vertex getNextVertex() {
            return this.next;
        }

        public void setVisited(boolean status) {
            this.visited = status;
        }

        public boolean isVisited() {
            return this.visited;
        }

        public void setDistance(int d) {
            this.distance = d;
        }

        public int getDistance() {
            return this.distance;
        }

        /**
         * Create a list of arcs of the vertex.
         *
         * @return arcs as list
         */
        public List<Arc> getVertexArcs() {
            final List<Arc> arcs = new ArrayList<>();
            for (Arc a = first; a != null; a = a.getNextArc()) {
                arcs.add(a);
            }
            return arcs;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    public class Arc {

        private String id;
        private Vertex target;
        private Arc next;

        /**
         * Constructor of arc with all parameters specialized.
         *
         * @param s as id of the arc
         * @param v as default next arc
         * @param e as arc between the arc and default next
         *          arc
         */
        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        /**
         * Constructor of a arc with only id specialized.
         *
         * @param s as id of the string
         */
        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        public Vertex getTarget() {
            return target;
        }

        public Arc getNextArc() {
            return next;
        }
    }


    public class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        /**
         * Constructor of graph with name and root vertex specialized.
         *
         * @param s as id of the root vertex's id
         * @param v as default root vertex
         */
        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        /**
         * Constructor of graph with only name specialized.
         *
         * @param s as id of the root vertex's id
         * @param v as default root vertex
         */
        Graph(String s) {
            this(s, null);
        }

        /**
         * Returns a string representation of the graph.
         *
         * @return id as id of the vertex
         */
        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Setting distance value to every vertex. The value depends
         * of the shortest path from source vertex.
         *
         * @param v as source vertex
         */
        private void setDistances(Vertex v) {
            v.setVisited(true);
            int dist = v.getDistance();

            for (Arc a : v.getVertexArcs()) {
                if (a.getTarget().getDistance() < dist - 1 && a.getTarget().isVisited()) {
                    v.setDistance(a.getTarget().getDistance() + 1);
                    dist = v.getDistance();
                }
            }
            for (Arc a : v.getVertexArcs()) {
                if (!a.getTarget().isVisited()) {
                    a.getTarget().setDistance(dist + 1);
                }
            }
            for (Arc a : v.getVertexArcs()) {
                if (!a.getTarget().isVisited()) {
                    setDistances(a.getTarget());
                }
            }
        }

        /**
         * Finding vertex from verteces list by vertex's id value.
         *
         * @param id
         * @return result as vertex
         */
        public Vertex getVertexById(String id) {
            for (Vertex result : allVertices()) {
                if (result.id.equals(id)) {
                    return result;
                }
            }
            throw new RuntimeException("No such vertex in graph: " + id);
        }

        /**
         * Create up list of verteces.
         *
         * @return list of verteces
         */
        public List<Vertex> allVertices() {
            final List<Vertex> vertices = new ArrayList<>();
            for (Vertex v = first; v != null; v = v.getNextVertex())
                vertices.add(v);
            return vertices;
        }

        /**
         * Create arraylist of farthest verteces. If there is only
         * one vertex in graph, the method return exceptions
         *
         * @param source as vertex
         * @return list of farthest verteces
         */
        public List<Vertex> farthestVertex(Vertex source) {
            Stack<Vertex> results = new Stack<>();
            setDistances(source);

            for (Vertex v : allVertices()) {
                if (results.isEmpty()) {
                    results.push(v);
                } else if (v.getDistance() > results.peek().getDistance()) {
                    results.pop();
                    results.push(v);
                } else if (v.getDistance() == results.peek().getDistance()) {
                    results.push(v);
                }
            }
            resetVerteces();
            return new ArrayList<>(results);
        }

        /**
         * Setting vertex's distance to zero and status visited to false.
         */
        private void resetVerteces() {
            for (Vertex v = first; v != null; v = v.getNextVertex()) {
                v.setVisited(false);
                v.setDistance(0);
            }
        }
    }
}

